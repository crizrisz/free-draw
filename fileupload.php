<?php
    header("Access-Control-Allow-Origin: *");
  
    $data = json_decode(file_get_contents("php://input"), true);
    // File name
    $filename = $_FILES['file']['name'];

    // Valid file extensions
    $valid_extensions = array("xlsx","xls","pdf");

    // File extension
    $extension = pathinfo($filename, PATHINFO_EXTENSION);


    $upload_path = 'uploads/' . $filename . '_' . time() . '.' . $extension;

    // if (!file_exists('path/to/directory')) {
    //     mkdir('path/to/directory', 0777, true);
    // }

    // Check extension
    if(in_array(strtolower($extension),$valid_extensions) ) {
        // Upload file
        if(move_uploaded_file($_FILES['file']['tmp_name'], $upload_path)){
            $status = 1;
            $message = 'File Uploaded';
            $image = $upload_path;
        }else{
            $status = 0;
            $message = 'There is an error while uploading file';
        }

    }else{
        $status = 0;
        $message = 'Only .xls, .xlsx and .pdf allowed to upload';
    }
    
    
    $response = array(
        "status" => $status,
        "message" => $message
    );
    
    echo json_encode($response);
    exit();
?>