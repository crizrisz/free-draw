<?php
    require('fpdf/fpdf.php');
    header("Access-Control-Allow-Origin: *");

    $con = mysqli_connect("localhost","root","","freedraw");
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }
    
    $data = json_decode(file_get_contents("php://input"), true);
    $id = isset($data['id']) ? $data['id'] : $_POST['id'];
    
    $sql = "SELECT * FROM userlog where id = ".$id;

    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0) {
    
        while($row = mysqli_fetch_assoc($result)) {
            $collect[] = array(
                "id" => $row["id"],
                "name" => $row["name"],
                "status" => $row["sign_status"],
                "pic" => $row["pic"]
            );
        }

        $status = 1;
        $message = "success";

    } else {
        
        $status = 0;
        $message = "no result";
        $collect = [];
    }

    
    $response = array(
        "status" => $status,
        "message" => $message,
        "data" => $collect
    );
    
    echo json_encode($response);
    mysqli_close($con);
    exit();
?>