<?php
    require('fpdf/fpdf.php');
    header("Access-Control-Allow-Origin: *");

    $con = mysqli_connect("localhost","root","","freedraw");
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $post_body = file_get_contents('php://input');
    $decoded_data = json_decode($post_body, true);
    $base64_code = $decoded_data["image"];
    $id = $decoded_data["id"];

    $filename = uniqid() . '.png';
    $output_file = 'images/' . $filename;

    $sql = "UPDATE userlog SET sign_status = 'y', pic = '".$filename."'  WHERE id=".$id;
    
    if (mysqli_query($con, $sql)) {

        $ifp = fopen( $output_file, 'wb' ); 
        $data = explode( ',', $base64_code );
    
        // we could add validation here with ensuring count( $data ) > 1
        // fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        $message = "";
        $status = null;
        if(fwrite( $ifp, base64_decode( $data[ 1 ] ) ) === false){
            $message = "writing failed";
            $status = 0;
        }else{
            $message = "writing successfull";
            $status = 1;
        }
        // clean up the file resource
        fclose( $ifp ); 
        

    } else {
        echo "Error updating record: " . mysqli_error($con);
    }

    $response = array(
        "status" => $status,
        "message" => $message,
    );

    echo json_encode($response);
    exit();
?>