<?php
    // require('fpdf/fpdf.php');
    require('fpdf/rotation.php');
    header("Access-Control-Allow-Origin: *");

    $con = mysqli_connect("localhost","root","","freedraw");
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
    }

    $sql = "SELECT * FROM userlog where sign_status = 'y'";

    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0) {
    
        while($row = mysqli_fetch_assoc($result)) {
            $collect[] = array(
                "id" => $row["id"],
                "name" => $row["name"],
                "status" => $row["sign_status"],
                "pic" => $row["pic"]
            );
        }

        $status = 1;
        $message = "success";

    } else {
        
        $status = 0;
        $message = "no result";
        $collect = [];
    }

    class PDF extends PDF_Rotate {
        function Header()
        {
            //Put the watermark
            $this->SetFont('Arial','B',50);
            $this->SetTextColor(255,192,203);
            $this->RotatedText(35,190,'W a t e r m a r k   d e m o',45);
        }

        function RotatedText($x, $y, $txt, $angle)
        {
            //Text rotated around its origin
            $this->Rotate($angle,$x,$y);
            $this->Text($x,$y,$txt);
            $this->Rotate(0);
        }
    }
    
        $pdf = new PDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16);
        $h = 0;
        foreach ($collect as $value) {
            # code...
            $output_file = 'images/' . $value['pic'];
            $pdf->Cell(40,40, 'username : '. $value['name'], 0 , 1 , 'L');
            $pdf->Image($output_file,30,10 + $h,30);
            $h = $h + 40;
        }
        // $pdf->Cell(40,40, 'username : '. $value['name'], 0 , 1 , 'L');
        // $pdf->Image($output_file,30,50,30);
        $txt="FPDF is a PHP class which allows to generate PDF files with pure PHP, that is to say ".
        "without using the PDFlib library. F from FPDF stands for Free: you may use it for any ".
        "kind of usage and modify it to suit your needs.\n\n";
        for($i=0;$i<5;$i++) 
            $pdf->MultiCell(0,5,$txt,0,'J');

        $pdfurl = "pdf_output/mypdf.pdf";
        $pdf->Output($pdfurl,"F");


    $response = array(
        "status" => $status,
        "message" => $message,
        "url" => $pdfurl
    );

    echo json_encode($response);
    exit();
?>